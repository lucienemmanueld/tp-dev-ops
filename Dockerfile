FROM openjdk:22-jdk
RUN addgroup --system spring && adduser --system --group spring
USER spring:spring
COPY /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
